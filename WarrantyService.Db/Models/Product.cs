﻿using System;
using System.Collections.Generic;

namespace WarrantyService.Db.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProductBatch { get; set; }
        public ICollection<Warranty> Warranty { get; set; }

        public Company Company { get; set; }
        public Guid CompanyId { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
