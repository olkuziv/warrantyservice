﻿using System;
using System.Collections.Generic;

namespace WarrantyService.Db.Models
{
    public class Company
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
