﻿using System;
using System.Collections.Generic;

namespace WarrantyService.Db.Models
{
    public class ServiceCenter
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<WarrantyIncident> WarrantyIncidents { get; set; }
        public ICollection<ServiceCenterUsers> Users { get; set; }
    }
}
