﻿using System;
using System.Collections.Generic;

namespace WarrantyService.Db.Models
{
    public class Client
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<Warranty> Warranty { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public UserType UserType { get; set; }
        public ICollection<ServiceCenterUsers> ServiceCenters { get; set; }
    }

    public enum UserType
    {
        Client,
        ServiceCenter
    }
}
