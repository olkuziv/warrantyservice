﻿using System;
using System.Collections.Generic;

namespace WarrantyService.Db.Models
{
    public class Warranty
    {
        public Guid Id { get; set; }
        public DateTime PurchasedDate { get; set; } = DateTime.Now;
        public DateTime WarrantyEndDate { get; set; } = DateTime.Now;

        public Client Client { get; set; }
        public Guid ClientId { get; set; }

        public Product Product { get; set; }
        public Guid ProductId { get; set; }
        public ICollection<WarrantyIncident> WarrantyIncident { get; set; } = new List<WarrantyIncident>();
    }
}
