﻿using System;

namespace WarrantyService.Db.Models
{
    public class WarrantyIncident
    {
        public Guid Id { get; set; }
        public string CrashDescription { get; set; }
        public Warranty Warranty { get; set; }
        public Guid WarrantyId { get; set; }
        public ServiceCenter ServiceCenter { get; set; }
        public Guid ServiceCenterId { get; set; }
        public DateTime DateGivingToService { get; set; }

        public bool IsApplied { get; set; }
        public DateTime CreationDate { get; set; }

        public bool IsFixed { get; set; }
        public DateTime DateFixed { get; set; }

        public bool Rejected { get; set; }
        public string ReasonOfRejection { get; set; }
    }
}
