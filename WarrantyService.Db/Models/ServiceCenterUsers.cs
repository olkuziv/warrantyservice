﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarrantyService.Db.Models
{
    public class ServiceCenterUsers
    {
        public ServiceCenter ServiceCenter { get; set; }
        public Client User { get; set; }

        public Guid ServiceCenterId { get; set; }
        public Guid UserId { get; set; }
    }
}
