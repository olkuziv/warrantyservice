﻿using Microsoft.EntityFrameworkCore;
using WarrantyService.Db.Models;

namespace WarrantyService.Db
{
    public class WarrantyDbContext : DbContext
    {
        public WarrantyDbContext(DbContextOptions<WarrantyDbContext> contextOptions) 
            : base(contextOptions)
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ServiceCenter> ServiceCenters { get; set; }
        public DbSet<Warranty> Warranties { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<WarrantyIncident> WarrantyIncidents { get; set; }
        public DbSet<ServiceCenterUsers> ServiceCenterUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServiceCenterUsers>()
                .HasKey(bc => new { bc.ServiceCenterId, bc.UserId});
            modelBuilder.Entity<ServiceCenterUsers>()
                .HasOne(bc => bc.ServiceCenter)
                .WithMany(b => b.Users)
                .HasForeignKey(bc => bc.ServiceCenterId);
            modelBuilder.Entity<ServiceCenterUsers>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.ServiceCenters)
                .HasForeignKey(bc => bc.UserId);
        }
    }
}
