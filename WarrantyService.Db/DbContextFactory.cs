﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace WarrantyService.Db
{
    public class DbContextFactory<T> where T : DbContext
    {
        private readonly Func<T> _creator;

        public DbContextFactory(Func<T> creator)
        {
            _creator = creator;
        }

        public T Get()
        {
            return _creator();
        }
    }
}
