﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WarrantyService.Core.States;
using WarrantyService.Db;
using WarrantyService.Db.Models;

namespace WarrantyService.Core.Services
{
    public class ProductsService
    {
        private readonly ProductsState _state;
        private readonly WarrantyDbContext _dbContext;

        public ProductsService(ProductsState state, WarrantyDbContext dbContext)
        {
            _state = state;
            _dbContext = dbContext;
        }

        public async Task Create(Warranty newProduct)
        {
            _dbContext.Warranties.Add(newProduct);
            _dbContext.SaveChanges();
            await _state.OnProductsListChanged();
        }

        public Product[] Search(string productName)
        {
            if (string.IsNullOrEmpty(productName))
                return _dbContext.Products.ToArray();

            return _dbContext.Products
                .Where(product => product.Name.ToLower().Contains(productName))
                .ToArray();
        }

        public Task<Warranty[]> GetUserWarranties(Guid userId)
        {
            return _dbContext.Warranties
                .Where(w => w.Client.Id == userId)
                .Include(warranty => warranty.Product)
                    .ThenInclude(product => product.Company)
                .Include(warranty => warranty.WarrantyIncident)
                .ToArrayAsync();
        }

        public Task<Warranty> Get(Guid warrantyId)
        {
            return _dbContext.Warranties
                .Include(warranty => warranty.Product)
                .Include(warranty => warranty.WarrantyIncident)
                    .ThenInclude(incident => incident.ServiceCenter)
                .FirstOrDefaultAsync(warranty => warranty.Id == warrantyId);
        }

        public async Task Delete(Warranty warranty)
        {
            _dbContext.Warranties.Remove(warranty);
            await _dbContext.SaveChangesAsync();
        }
    }
}
