﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarrantyService.Db;
using WarrantyService.Db.Models;

namespace WarrantyService.Core.Services
{
    public class WarrantyServicesService
    {
        private readonly DbContextFactory<WarrantyDbContext> _dbContextFactory;

        public WarrantyServicesService(DbContextFactory<WarrantyDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public ServiceCenter[] GetCenters()
        {
            using (var dbContext = _dbContextFactory.Get())
            {
                return dbContext.ServiceCenters
                    .Include(center => center.Users)
                    .ToArray();
            }
        }

        public ServiceCenter[] Search(string name)
        {
            using (var dbContext = _dbContextFactory.Get())
            {
                if (string.IsNullOrEmpty(name))
                    return GetCenters();

                name = name.ToLower();
                return dbContext.ServiceCenters
                    .Include(center => center.Users)
                    .Where(center => center.Name.ToLower().Contains(name))
                    .ToArray();

            }
        }
    }
}
