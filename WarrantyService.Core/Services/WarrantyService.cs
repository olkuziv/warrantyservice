﻿using System.Threading.Tasks;
using WarrantyService.Core.States;
using WarrantyService.Db;
using WarrantyService.Db.Models;

namespace WarrantyService.Core.Services
{
    public class WarrantyService
    {
        private readonly WarrantyDbContext _dbContext;
        private readonly ProductsState _productsState;

        public WarrantyService(WarrantyDbContext dbContext, ProductsState productsState)
        {
            _dbContext = dbContext;
            _productsState = productsState;
        }

        public async Task CreateIncident(Warranty warranty, ServiceCenter serviceCenter, string crashDescription)
        {
            _dbContext.WarrantyIncidents.Add(new WarrantyIncident
            {
                WarrantyId = warranty.Id,
                ServiceCenterId = serviceCenter.Id,
                CrashDescription = crashDescription
            });
            await _dbContext.SaveChangesAsync();
            await _productsState.OnProductsListChanged();
        }

        public async Task Update(WarrantyIncident warrantyIncident)
        {
             _dbContext.WarrantyIncidents.Update(warrantyIncident);
             await _dbContext.SaveChangesAsync();
        }
    }
}
