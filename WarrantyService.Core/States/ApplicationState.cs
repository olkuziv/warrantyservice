﻿using System.Threading.Tasks;

namespace WarrantyService.Core.States
{
    public class ApplicationState
    {
        public delegate Task OnPageChangedHandler(string pageName);

        public delegate Task OnRedrawDelegate();

        public event OnPageChangedHandler PageChanged;
        public event OnRedrawDelegate Redraw;

        public virtual Task OnPageChanged(string pageName)
        {
            return PageChanged?.Invoke(pageName) ?? Task.CompletedTask;
        }

        public virtual Task OnRedraw()
        {
            return Redraw?.Invoke() ?? Task.CompletedTask;
        }
    }
}
