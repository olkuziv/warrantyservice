﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WarrantyService.Db;
using WarrantyService.Db.Models;

namespace WarrantyService.Core.States
{
    public class AuthState
    {
        private readonly WarrantyDbContext _dbContext;

        public AuthState(WarrantyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Client Client { get; set; }

        public async Task<Client> Login(string login, string password)
        {
            Client = await _dbContext.Clients.FirstOrDefaultAsync(client => 
                client.Login == login && client.Password == password);
            if (Client == null)
                return null;

            await OnAuthStateChanged(true);
            return Client;
        }

        public async Task Logout()
        {
            Client = null;
            await OnAuthStateChanged(false);
        }

        public delegate Task AuthStateChangedDelegate(bool isLogin);

        public event AuthStateChangedDelegate AuthStateChanged;

        protected virtual Task OnAuthStateChanged(bool isLogin)
        {
            return AuthStateChanged?.Invoke(isLogin) ?? Task.CompletedTask;
        }
    }
}
