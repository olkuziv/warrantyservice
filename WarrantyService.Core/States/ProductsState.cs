﻿using System.Threading.Tasks;

namespace WarrantyService.Core.States
{
    public class ProductsState
    {
        public ProductsState(AuthState authState)
        {
            authState.AuthStateChanged += AuthState_AuthStateChanged;
        }

        private Task AuthState_AuthStateChanged(bool isLogin)
        {
            return OnProductsListChanged();
        }


        public delegate Task ProductsListChangedHandler();
        public event ProductsListChangedHandler ProductsListChanged;

        public virtual Task OnProductsListChanged()
        {
            return ProductsListChanged?.Invoke() ?? Task.CompletedTask;
        }
    }
}
