﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WarrantyService.Core.Services;
using WarrantyService.Core.States;
using WarrantyService.Db;
using WarrantyService.Db.Models;
using WarrantyService.UI.Abstractions;
using WarrantyService.UI.Pages;
using WarrantyService.UI.Pages.Client;
using WarrantyService.UI.Pages.Service;
using WarrantyService.UI.ViewModels;
using WarrantyService.UI.Windows;

namespace WarrantyService.UI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<LayoutWindow>();

            services.AddTransient<LoginPage>();
            services.AddTransient<AddMyProductPage>();
            services.AddTransient<ProductsPage>();
            services.AddSingleton<ProductPage>();
            services.AddTransient<LogoutPage>();

            services.AddTransient<ServiceIncidentsPage>();

            services.AddTransient<IProductsPageViewModel, ClientProductsViewModel>();
            services.AddSingleton<IProductPageViewModel, ClientProductViewModel>();

            services.AddSingleton<ApplicationState>();
            services.AddSingleton<AuthState>();
            services.AddSingleton<ProductsState>();

            services.AddTransient<ProductsService>();
            services.AddSingleton<WarrantyServicesService>();
            services.AddSingleton<Core.Services.WarrantyService>();

            services.AddSingleton(provider => new DbContextFactory<WarrantyDbContext>(provider.GetRequiredService<WarrantyDbContext>));
            services.AddDbContext<WarrantyDbContext>(builder =>
                builder.UseSqlServer(_configuration.GetConnectionString("WarrantyDb")), ServiceLifetime.Transient, ServiceLifetime.Transient);
        }

        public async Task Pipeline(IServiceProvider provider)
        {
            var context = provider.GetRequiredService<WarrantyDbContext>();
            context.Database.Migrate();
            if (!context.Companies.Any())
                SeedData(context);

            var mainWindow = provider.GetRequiredService<LayoutWindow>();
            mainWindow.Show();

            var authState = provider.GetRequiredService<AuthState>();
            //await authState.Login("service", "service");
            //await authState.Login("admin", "admin");
        }

        public void SeedData(WarrantyDbContext context)
        {
            var companies = new[]
            {
                new Company
                {
                    Name = "Apple",
                    Products = new List<Product>
                    {
                        new Product{ Name = "Iphone 4" },
                        new Product{ Name = "Iphone 5" },
                        new Product{ Name = "Iphone 6" },
                        new Product{ Name = "Iphone 7" },
                        new Product{ Name = "Iphone 8" },
                        new Product{ Name = "Iphone X" },
                    }
                },
                new Company
                {
                    Name = "Samsung",
                    Products = new List<Product>
                    {
                        new Product{ Name = "Galaxy S4" },
                        new Product{ Name = "Galaxy S7" },
                        new Product{ Name = "Galaxy S9" },
                        new Product{ Name = "Galaxy Note10" },
                    }
                },
                new Company
                {
                    Name = "Meizu",
                    Products = new List<Product>
                    {
                        new Product{ Name = "M5" },
                        new Product{ Name = "M8" },
                        new Product{ Name = "X8" },
                        new Product{ Name = "Note 8" },
                    }
                },
                new Company
                {
                    Name = "Huawei",
                    Products = new List<Product>
                    {
                        new Product{ Name = "P30" },
                        new Product{ Name = "P30 Pro" },
                        new Product{ Name = "P30 Lite" },
                        new Product{ Name = "P smart Z" },
                        new Product{ Name = "HUAWEI MateBook D" },
                        new Product{ Name = "Mediapad t3 10" },
                    }
                },
                new Company
                {
                    Name = "Google",
                    Products = new List<Product>
                    {
                        new Product{ Name = "Pixel" },
                        new Product{ Name = "Pixel XL" },
                    }
                },
                new Company
                {
                    Name = "Xiaomi",
                    Products = new List<Product>
                    {
                        new Product{ Name = "Mi Mix 3" },
                        new Product{ Name = "Mi 9" },
                        new Product{ Name = "Mi 9T" },
                        new Product{ Name = "Mi 9 SE" },
                        new Product{ Name = "Mi A2" },
                        new Product{ Name = "Mi A3" },
                        new Product{ Name = "Redmi Note 5" },
                        new Product{ Name = "Redmi Note 7" },
                    }
                },
            };
            context.Clients.AddRange(
                new Client
                {
                    Login = "admin",
                    Password = "admin",
                    UserType = UserType.Client
                }
            );
            context.ServiceCenters.Add(new ServiceCenter
            {
                Name = "Apple Fix",
                PhoneNumber = "88003553535",
                Address = "Herbert Wells street, 22",
                Users = new List<ServiceCenterUsers>
                {
                    new ServiceCenterUsers
                    {
                        User = new Client
                        {
                            Login = "Service",
                            Password = "Service",
                            UserType = UserType.ServiceCenter
                        }
                    }
                }
            });
            context.Companies.AddRange(companies);
            context.SaveChanges();
        }
    }
}
