﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using Microsoft.EntityFrameworkCore;
using WarrantyService.Core.States;
using WarrantyService.Db;
using WarrantyService.UI.Annotations;
using WarrantyService.UI.Pages;
using WarrantyService.UI.Pages.Client;
using WarrantyService.UI.Pages.Service;

namespace WarrantyService.UI.Windows
{
    public partial class LayoutWindow : Window, INotifyPropertyChanged
    {
        private readonly AuthState _authState;
        private readonly ApplicationState _applicationState;
        private readonly WarrantyDbContext _context;
        private readonly ServiceIncidentsPage _incidentsPage;
        private readonly LoginPage _loginPage;
        private readonly AddMyProductPage _addMyProductPage;
        private readonly ProductsPage _productsPage;
        private readonly ProductPage _productPage;
        private readonly Page _logoutPage;
        public LayoutWindow(AuthState authState, ApplicationState applicationState, WarrantyDbContext context,
            ServiceIncidentsPage incidentsPage,
            LoginPage loginPage, AddMyProductPage addMyProductPage, ProductsPage productsPage, ProductPage productPage, LogoutPage logoutPage)
        {
            InitializeComponent();
            DataContext = this;

            _authState = authState;
            _applicationState = applicationState;
            _context = context;
            _incidentsPage = incidentsPage;
            _loginPage = loginPage;
            _addMyProductPage = addMyProductPage;
            _productsPage = productsPage;
            _productPage = productPage;
            _logoutPage = logoutPage;

            

            _authState.AuthStateChanged += AuthStateChanged;
            _applicationState.PageChanged += ApplicationState_OnPageChanged;
            _applicationState.Redraw += ApplicationState_OnRedraw;
            Snackbar = MainSnackbar;
            _applicationState.OnPageChanged(nameof(LoginPage));
        }

        public bool IsAuthorized
        {
            get => _isAuthorized;
            set
            {
                _isAuthorized = value;
                OnPropertyChanged(nameof(IsAuthorized));
            }
        }

        private async Task AuthStateChanged(bool isLogin)
        {
            if (isLogin)
            {
                if (await _context.ServiceCenterUsers.AnyAsync(users => users.UserId == _authState.Client.Id))
                {
                    Pages = new ObservableCollection<Page>()
                    {
                        _incidentsPage
                    };
                    await _applicationState.OnPageChanged(nameof(ServiceIncidentsPage));
                }
                else
                {
                    Pages = new ObservableCollection<Page>
                    {
                        _addMyProductPage,
                        _productsPage,
                    };
                    await _applicationState.OnPageChanged(nameof(ProductsPage));
                }
            }
            else
            {
                await _applicationState.OnPageChanged(nameof(LoginPage));
            }

            IsAuthorized = isLogin;
        }

        private Task ApplicationState_OnRedraw()
        {
            //Height++;
            //OnPropertyChanged(nameof(Height));
            //Height--;
            //OnPropertyChanged(nameof(Height));
            return Task.CompletedTask;
        }

        public bool IsLeftMenuOpen
        {
            get => _isLeftMenuOpen;
            set
            {
                _isLeftMenuOpen = value;
                OnPropertyChanged(nameof(IsLeftMenuOpen));
            }
        }

        private async Task ApplicationState_OnPageChanged(string pageName)
        {
            switch (pageName)
            {
                case nameof(LoginPage):
                    SelectedPage = _loginPage;
                    break;
                case nameof(ProductsPage):
                    SelectedPage = _productsPage;
                    break;
                case nameof(AddMyProductPage):
                    SelectedPage = _addMyProductPage;
                    break;
                case nameof(ProductPage):
                    SelectedPage = _productPage;
                    break;
                case nameof(ServiceIncidentsPage):
                    SelectedPage = _incidentsPage;
                    break;
                case nameof(LogoutPage):
                    await _authState.Logout();
                    break;
            }
        }

        public ObservableCollection<Page> Pages
        {
            get => _pages;
            set
            {
                _pages = value;
                OnPropertyChanged(nameof(Pages));
            }
        }

        private Page _selectedPage;
        private bool _isLeftMenuOpen;
        private ObservableCollection<Page> _pages;
        private bool _isAuthorized;

        public Page SelectedPage
        {
            get => _selectedPage;
            set
            {
                if(value == null)
                    return;

                IsLeftMenuOpen = false;
                _selectedPage = value;
                OnPropertyChanged(nameof(SelectedPage));
            }
        }

        public static Snackbar Snackbar;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void Logout_OnClick(object sender, RoutedEventArgs e)
        {
            await ApplicationState_OnPageChanged(nameof(LogoutPage));
        }
    }
}
