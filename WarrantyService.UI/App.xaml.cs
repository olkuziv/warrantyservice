﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Windows;
using Microsoft.Extensions.Configuration;

namespace WarrantyService.UI
{
    public partial class App : Application
    {
        public App()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true);

            var configuration = builder.Build();

            StartupConfig = new Startup(configuration);

            ServiceCollection = new ServiceCollection();
            StartupConfig.ConfigureServices(ServiceCollection);

            ServiceProvider = ServiceCollection.BuildServiceProvider();
        }
        public IServiceProvider ServiceProvider { get; private set; }
        public Startup StartupConfig { get; set; }
        public IServiceCollection ServiceCollection { get; set; }

        protected override async void OnStartup(StartupEventArgs e)
        {
            await StartupConfig.Pipeline(ServiceProvider);
        }
    }
}
