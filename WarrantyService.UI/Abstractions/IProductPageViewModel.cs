﻿using System.Threading.Tasks;
using WarrantyService.Db.Models;

namespace WarrantyService.UI.Abstractions
{
    public interface IProductPageViewModel
    {
        ServiceCenter[] Services { get; }
        ServiceCenter SelectedServiceCenter { get; set; }
        string SearchRequest { get; set; }
        bool IsEnabledWarranty { get; }
        string WarrantyIncidentDescription { get; set; }
        Task UseWarranty();
        Warranty Warranty { get; set; }
        Task OpenProductPage(Warranty product);
    }
}
