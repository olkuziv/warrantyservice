﻿using System.Threading.Tasks;
using WarrantyService.Db.Models;

namespace WarrantyService.UI.Abstractions
{
    public interface IProductsPageViewModel
    {
        Warranty[] Products { get; set; }
        void OpenProductButton_OnClick(Warranty warranty);
        void DeleteProductButton_OnClick(Warranty warranty);
        Task ProductsState_OnProductsListChanged();
    }
}
