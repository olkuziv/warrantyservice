﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Threading;
using MaterialDesignThemes.Wpf;
using WarrantyService.Core.Services;
using WarrantyService.Core.States;
using WarrantyService.Db.Models;
using WarrantyService.UI.Abstractions;
using WarrantyService.UI.Annotations;

namespace WarrantyService.UI.ViewModels
{
    public class ClientProductViewModel : IProductPageViewModel, INotifyPropertyChanged
    {
        private readonly ApplicationState _applicationState;
        private readonly AuthState _authState;
        private readonly Core.Services.WarrantyService _warrantyService;
        private readonly WarrantyServicesService _servicesService;
        private readonly ProductsService _productsService;

        public ClientProductViewModel(ApplicationState applicationState, AuthState authState,
            Core.Services.WarrantyService warrantyService, WarrantyServicesService servicesService, ProductsService productsService)
        {
            _applicationState = applicationState;
            _authState = authState;
            _warrantyService = warrantyService;
            _servicesService = servicesService;
            _productsService = productsService;
            Warranty = new Warranty();
        }

        #region Search
        public ServiceCenter[] Services => _servicesService.Search(SearchRequest);
        public ServiceCenter SelectedServiceCenter { get; set; }
        public string SearchRequest { get; set; }

        public bool IsEnabledWarranty
        {
            get
            {
                var incident = Warranty.WarrantyIncident.LastOrDefault();
                if (incident == null)
                    return true;
                return incident.Rejected || incident.IsFixed;
            }
        }

        #endregion

        public IEnumerable<WarrantyIncident> WarrantyIncidents => Warranty.WarrantyIncident;
        private Warranty _warranty;
        public Warranty Warranty
        {
            get => _warranty;
            set
            {
                _warranty = value;
                OnPropertyChanged(nameof(Warranty));
                OnPropertyChanged(nameof(WarrantyIncidents));
                OnPropertyChanged(nameof(IsEnabledWarranty));
                _applicationState.OnRedraw().GetAwaiter().GetResult();
            }
        }

        public async Task OpenProductPage(Warranty product)
        {
            Warranty = await _productsService.Get(product.Id);
        }

        public string WarrantyIncidentDescription { get; set; }
        public async Task UseWarranty()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
            await _warrantyService.CreateIncident(Warranty, SelectedServiceCenter, WarrantyIncidentDescription);
            await OpenProductPage(Warranty);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
