﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using WarrantyService.Core.Services;
using WarrantyService.Core.States;
using WarrantyService.Db.Models;
using WarrantyService.UI.Abstractions;
using WarrantyService.UI.Annotations;
using WarrantyService.UI.Pages.Client;

namespace WarrantyService.UI.ViewModels
{
    public class ClientProductsViewModel : IProductsPageViewModel, INotifyPropertyChanged
    {
        private readonly ProductsService _productsService;
        private readonly ProductPage _productPage;
        private readonly AuthState _authState;
        private readonly ProductsState _productsState;
        private readonly ApplicationState _applicationState;

        public ClientProductsViewModel(ProductsService productsService, ProductPage productPage,
            AuthState authState, ProductsState productsState, ApplicationState applicationState)
        {
            _productsService = productsService;
            _productPage = productPage;
            _authState = authState;
            _applicationState = applicationState;
            _productsState = productsState;
            _productsState.ProductsListChanged += ProductsState_OnProductsListChanged;
            _productsState.OnProductsListChanged();
        }

        private Warranty[] _productsList;
        public Warranty[] Products
        {
            get => _productsList;
            set
            {
                _productsList = value;
                OnPropertyChanged(nameof(Products));
            }
        }
        public void OpenProductButton_OnClick(Warranty warranty)
        {
            _productPage.OpenProductPage(warranty);
            _applicationState.OnPageChanged(nameof(ProductPage));
        }

        public async void DeleteProductButton_OnClick(Warranty warranty)
        {
            await _productsService.Delete(warranty);
            await _productsState.OnProductsListChanged();
        }

        public async Task ProductsState_OnProductsListChanged()
        {
            await Load();
        }

        public async Task Load()
        {
            var products = _authState.Client == null 
                ? new Warranty[0] 
                : await _productsService.GetUserWarranties(_authState.Client.Id);
            Products = products;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
