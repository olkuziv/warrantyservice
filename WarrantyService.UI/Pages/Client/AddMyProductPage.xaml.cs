﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WarrantyService.Core.Services;
using WarrantyService.Core.States;
using WarrantyService.Db.Models;
using WarrantyService.UI.Annotations;

namespace WarrantyService.UI.Pages.Client
{
    public partial class AddMyProductPage : Page, INotifyPropertyChanged
    {
        private readonly ProductsService _productsService;
        private readonly AuthState _authState;
        private readonly ApplicationState _applicationState;
        private string _searchRequest;
        private Product _selectedProduct;
        private Product[] _products = {new Product()};

        public AddMyProductPage(ProductsService productsService, 
            AuthState authState, ApplicationState applicationState)
        {
            InitializeComponent();
            DataContext = this;

            _productsService = productsService;
            _authState = authState;
            _authState.AuthStateChanged += AuthStateChanged;
            _applicationState = applicationState;
            //_authState.UserLoginSuccess += AuthStateOnUserLoginSuccess;
        }

        private Task AuthStateChanged(bool isLogin)
        {
            if (isLogin)
            {
                IsEnabled = true;
                ResetNewProduct();
                Products = _productsService.Search("");
                return Task.CompletedTask;
            }

            IsEnabled = false;
            return Task.CompletedTask;
        }

        public Product[] Products
        {
            get => _products;
            set
            {
                _products = value;
                OnPropertyChanged(nameof(Products));
            }
        }

        public Product SelectedProduct
        {
            get => _selectedProduct;
            set
            {
                if(value == null)
                    return;

                _selectedProduct = value;
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }

        public Warranty NewWarranty{ get; set; }

        public string SearchRequest
        {
            get => _searchRequest;
            set
            {
                _searchRequest = value;
                OnPropertyChanged(nameof(Products));
            }
        }

        private async void AddMyProductButton_OnClick(object sender, RoutedEventArgs e)
        {
            NewWarranty.ClientId = _authState.Client.Id;
            NewWarranty.ProductId = SelectedProduct.Id;
            await _productsService.Create(NewWarranty);
            await _applicationState.OnPageChanged(nameof(ProductsPage));
            ResetNewProduct();
        }

        private void ResetNewProduct()
        {
            NewWarranty = new Warranty();
            SelectedProduct = new Product();
            SearchRequest = "";
            OnPropertyChanged(nameof(SearchRequest));
            OnPropertyChanged(nameof(SelectedProduct));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
