﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WarrantyService.Core.States;
using WarrantyService.Db.Models;
using WarrantyService.UI.Abstractions;

namespace WarrantyService.UI.Pages.Client
{
    public partial class ProductPage : Page
    {
        private readonly AuthState _authState;
        private readonly ApplicationState _applicationState;
        private readonly IProductPageViewModel _productPageViewModel;

        public ProductPage(AuthState authState, ApplicationState applicationState, 
            IProductPageViewModel productPageViewModelFactory) 
        {
            InitializeComponent();

            _authState = authState;
            _authState.AuthStateChanged += AuthStateChanged;
            _applicationState = applicationState;
            _productPageViewModel = productPageViewModelFactory;
        }

        private Task AuthStateChanged(bool isLogin)
        {
            if (!isLogin)
                return Task.CompletedTask;

            DataContext = _productPageViewModel;

            return Task.CompletedTask;
        }

        public async void OpenProductPage(Warranty product)
        {
            await _productPageViewModel.OpenProductPage(product);
        }

        private async void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            await _applicationState.OnPageChanged(nameof(ProductsPage));
        }

        private async void UseWarrantyButton_OnClick(object sender, RoutedEventArgs e)
        {
            await _productPageViewModel.UseWarranty();
        }
    }
}
