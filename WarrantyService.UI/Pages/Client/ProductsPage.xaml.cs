﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WarrantyService.Core.States;
using WarrantyService.Db.Models;
using WarrantyService.UI.Abstractions;

namespace WarrantyService.UI.Pages.Client
{
    public partial class ProductsPage : Page
    {
        private readonly IProductsPageViewModel _productsPageViewModel;
        private readonly AuthState _authState;

        public ProductsPage(IProductsPageViewModel productsPageViewModel, AuthState authState)
        {
            InitializeComponent();

            _productsPageViewModel = productsPageViewModel;
            _authState = authState;
            authState.AuthStateChanged+=AuthStateChanged;
        }

        private Task AuthStateChanged(bool isLogin)
        {
            if (!isLogin)
                return Task.CompletedTask;

            DataContext = _productsPageViewModel;

            return Task.CompletedTask;
        }

        private void DeleteProductButton_OnClick(object sender, RoutedEventArgs e)
        {
            var product = ((FrameworkElement)sender).DataContext as Warranty;
            _productsPageViewModel.DeleteProductButton_OnClick(product);
        }
        private void OpenProductButton_OnClick(object sender, RoutedEventArgs e)
        {
            var product = ((FrameworkElement)sender).DataContext as Warranty;
            _productsPageViewModel.OpenProductButton_OnClick(product);
        }
    }
}
