﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Microsoft.EntityFrameworkCore;
using WarrantyService.Core.States;
using WarrantyService.Db;
using WarrantyService.Db.Models;
using WarrantyService.UI.Annotations;

namespace WarrantyService.UI.Pages.Service
{
    public partial class ServiceIncidentsPage : Page, INotifyPropertyChanged
    {
        private readonly WarrantyDbContext _context;
        private readonly AuthState _authState;
        private WarrantyIncident[] _warrantyIncidents;

        public ServiceIncidentsPage(WarrantyDbContext context, AuthState authState)
        {
            _context = context;
            _authState = authState;
            _authState.AuthStateChanged += AuthStateChanged;
            InitializeComponent();
            DataContext = this;
        }

        private async Task AuthStateChanged(bool isLogin)
        {
            if (isLogin)
            {
                await ReloadData();
            }
        }

        private async Task ReloadData()
        {
            var service = await _context.ServiceCenterUsers
                .FirstOrDefaultAsync(users => users.UserId == _authState.Client.Id);

            WarrantyIncidents = await _context.WarrantyIncidents
                .Where(incident => incident.ServiceCenterId == service.ServiceCenterId)
                .Include(incident => incident.Warranty)
                .ThenInclude(warranty => warranty.Product)
                .ThenInclude(warranty => warranty.Company)
                .ToArrayAsync();
        }

        public WarrantyIncident[] WarrantyIncidents
        {
            get => _warrantyIncidents;
            set
            {
                _warrantyIncidents = value;
                OnPropertyChanged(nameof(WarrantyIncidents));
            }
        }

        private async void CloseIncidentButton_OnClick(object sender, RoutedEventArgs e)
        {
            var incident = (WarrantyIncident)((FrameworkElement)sender).DataContext;
            //var inc = WarrantyIncidents.First(warrantyIncident => warrantyIncident.Id == incident.Id);
            _context.Attach(incident);
            incident.IsFixed = true;
            _context.SaveChanges();
            await ReloadData();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void AcceptIncidentButton_OnClick(object sender, RoutedEventArgs e)
        {
            var incident =(WarrantyIncident)((FrameworkElement)sender).DataContext;
            _context.Attach(incident);
            incident.IsApplied = true;
            _context.SaveChanges();
            await ReloadData();
        }

        private async void DiscardIncidentButton_OnClick(object sender, RoutedEventArgs e)
        {
            var incident = (WarrantyIncident)((FrameworkElement)sender).DataContext;
            _context.Attach(incident);
            incident.Rejected = true;
            _context.SaveChanges();
            await ReloadData();
        }
    }
}
