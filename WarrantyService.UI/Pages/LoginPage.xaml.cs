﻿using System.Windows;
using System.Windows.Controls;
using WarrantyService.Core.States;
using WarrantyService.UI.Windows;

namespace WarrantyService.UI.Pages
{
    public partial class LoginPage : Page
    {
        private readonly AuthState _authState;
        private readonly ApplicationState _applicationState;

        public LoginPage(AuthState authState, ApplicationState applicationState)
        {
            _authState = authState;
            _applicationState = applicationState;
            InitializeComponent();
            DataContext = this;
        }
        

        public string Login { get; set; }
        public string Password { get; set; }

        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            var user = await _authState.Login(Login, Password);
            if (user == null)
            {
                LayoutWindow.Snackbar.MessageQueue.Enqueue("User not found");
                return;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = ((PasswordBox)sender).Password;
        }
    }
}
